<?php

$servername = '127.0.0.1';
$username = 'root';
$password = '';
$dbname = 'presidents';

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>
<!DOCTYPE html>
<HTMl>
<head>
    <title>Presidential Search</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }
        .center-wrapper {
            text-align: center;
        }
        #top-div {
            padding-top: 30%;
        }
        #bottom-div {
            padding-top: 5%;
        }
        #search {
            width: 50%;
            height: 50px;
            display: inline-block;
            font-size: 1.25em;
            border-radius: 5px;
        }
        #table {
            display: inline-block;
        }
        #fl-left {
            text-align: left;
            padding-right: 40px;
        }
    </style>
</head>
<body>
    <div class="center-wrapper" id="top-div">
        <h1>Presedential Search</h1>
        <form name="form" action="" method="get">
            <input id="search" name="search-pres" type="text" placeholder="Search a president..." value ="<?php echo htmlspecialchars($_GET['search-pres']);?>"/>
            <?php
            $search_input = $_GET['search-pres'];
            if ($search_input != '' || $search_input != NULL) {
                $sql = "SELECT * FROM presidents WHERE name LIKE '%$search_input%' ORDER BY name ASC";
                $result = $conn->query($sql);
            }
            ?>
        </form>
    </div>
    <div class="center-wrapper" id="bottom-div">
    <?php

    echo "<table id=\"table\">"; // start a table tag in the HTML
    if ($search_input != '' || $search_input != NULL) {
        while($row = mysqli_fetch_array($result)){   //Creates a loop to loop through results
            echo "<tr><td id='fl-left'>" . $row['name'] . "</td><td>" . $row['start'] . "</td><td> - " . $row['end'] . "</td></tr>";
        }
    }

    echo "</table>"; //Close the table in HTML
    ?>
    </div>
</body>
</HTMl>
