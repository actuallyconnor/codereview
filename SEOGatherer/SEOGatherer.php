<?php
/*function robots_exists($url) {
    $url = $url."/robots.txt";
    $file_headers = @get_headers($url);
    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $exists = false;
    }
    else {
        $exists = true;
    }
    return $exists;
}

function sitemap_exits($url) {
    $url = $url."/sitemap.xml";
    $file_headers = @get_headers($url);
    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $exists = false;
    }
    else {
        $exists = true;
    }
    return $exists;
}*/

function load_xml() {
    $url = 'https://www.ansible.com/sitemap.xml';
    $xmldata = simplexml_load_file($url, "SimpleXMLElement",LIBXML_NOCDATA);
    if ($xmldata === false) {
        echo "Failed to load XML file from site";
    } else {
        $all_urls = array();
        foreach ($xmldata->children() as $locs) {
            array_push($all_urls, strval($locs->loc));
        }
        //print_r($all_urls);
        return $all_urls;
    }
}

function get_meta() {
    $urls = load_xml();
    foreach ($urls as $u) {
        $tags = get_meta_tags($u);
        print_r($tags);
        echo "\n";
    }
    $tags = get_meta_tags($urls[0], true);
    print_r($tags);
    echo "\n";
}

get_meta();

echo "\n";

