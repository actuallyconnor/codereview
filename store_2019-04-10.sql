# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.15)
# Database: store
# Generation Time: 2019-04-10 17:36:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Customer_Payment_Methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Customer_Payment_Methods`;

CREATE TABLE `Customer_Payment_Methods` (
  `customer_payment_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `payment_method_code` int(11) DEFAULT NULL,
  `credit_card_number` float DEFAULT NULL,
  `payment_method_details` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`customer_payment_id`),
  KEY `payment_method_code` (`payment_method_code`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_payment_methods_ibfk_1` FOREIGN KEY (`payment_method_code`) REFERENCES `ref_payment_methods` (`payment_method_code`),
  CONSTRAINT `customer_payment_methods_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Customers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Customers`;

CREATE TABLE `Customers` (
  `customer_id` int(11) NOT NULL,
  `organization_or_person` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `organization_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `middle_initial` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `login_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `login_password` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address_line_1` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address_line_2` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address_line_3` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address_line_4` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `province_state` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Invoices`;

CREATE TABLE `Invoices` (
  `invoice_number` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `invoice_status_code` int(11) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_details` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`invoice_number`),
  KEY `order_id` (`order_id`),
  KEY `invoice_status_code` (`invoice_status_code`),
  CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
  CONSTRAINT `invoices_ibfk_2` FOREIGN KEY (`invoice_status_code`) REFERENCES `ref_invoice_status_codes` (`invoice_status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Order_Items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Order_Items`;

CREATE TABLE `Order_Items` (
  `order_item_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_item_status_code` int(11) DEFAULT NULL,
  `order_item_quantity` int(11) DEFAULT NULL,
  `order_item_price` float DEFAULT NULL,
  `RMA_number` int(11) DEFAULT NULL,
  `RMA_issued_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `RMA_issued_date` date DEFAULT NULL,
  `other_ordered_items_details` tinytext COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`order_item_id`),
  KEY `product_id` (`product_id`),
  KEY `order_item_status_code` (`order_item_status_code`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`order_item_status_code`) REFERENCES `ref_order_item_status_codes` (`order_item_status_code`),
  CONSTRAINT `order_items_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Orders`;

CREATE TABLE `Orders` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `order_status_code` int(11) DEFAULT NULL,
  `date_order_placed` date DEFAULT NULL,
  `order_details` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`order_id`),
  KEY `order_status_code` (`order_status_code`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`order_status_code`) REFERENCES `ref_order_status_codes` (`order_status_code`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Payments`;

CREATE TABLE `Payments` (
  `payment_id` int(11) NOT NULL,
  `invoice_number` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` float DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `invoice_number` (`invoice_number`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`invoice_number`) REFERENCES `invoices` (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Products`;

CREATE TABLE `Products` (
  `product_id` int(11) NOT NULL,
  `product_type_code` int(11) DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_general_ci,
  `other_product_details` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`product_id`),
  KEY `product_type_code` (`product_type_code`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`product_type_code`) REFERENCES `ref_product_types` (`product_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Ref_Invoice_Status_Codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ref_Invoice_Status_Codes`;

CREATE TABLE `Ref_Invoice_Status_Codes` (
  `invoice_status_code` int(11) NOT NULL,
  `invoice_status_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`invoice_status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Ref_Order_Item_Status_Codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ref_Order_Item_Status_Codes`;

CREATE TABLE `Ref_Order_Item_Status_Codes` (
  `order_item_status_code` int(11) NOT NULL,
  `order_item_status_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`order_item_status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Ref_Order_Status_Codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ref_Order_Status_Codes`;

CREATE TABLE `Ref_Order_Status_Codes` (
  `order_status_code` int(11) NOT NULL,
  `order_status_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`order_status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Ref_Payment_Methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ref_Payment_Methods`;

CREATE TABLE `Ref_Payment_Methods` (
  `payment_method_code` int(11) NOT NULL,
  `payment_method_description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`payment_method_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Ref_Product_Types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ref_Product_Types`;

CREATE TABLE `Ref_Product_Types` (
  `product_type_code` int(11) NOT NULL,
  `parent_product_type_code` int(11) DEFAULT NULL,
  `product_type_description` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`product_type_code`),
  KEY `parent_product_type_code` (`parent_product_type_code`),
  CONSTRAINT `ref_product_types_ibfk_1` FOREIGN KEY (`parent_product_type_code`) REFERENCES `ref_product_types` (`product_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Shipment_Items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Shipment_Items`;

CREATE TABLE `Shipment_Items` (
  `shipment_id` int(11) NOT NULL,
  `order_item_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`shipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table Shipments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Shipments`;

CREATE TABLE `Shipments` (
  `shipment_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `invoice_number` int(11) DEFAULT NULL,
  `shipment_tracking_number` int(11) DEFAULT NULL,
  `shipment_date` datetime DEFAULT NULL,
  `other_shipment_details` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`shipment_id`),
  KEY `order_id` (`order_id`),
  KEY `invoice_number` (`invoice_number`),
  CONSTRAINT `shipments_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
  CONSTRAINT `shipments_ibfk_2` FOREIGN KEY (`invoice_number`) REFERENCES `invoices` (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
